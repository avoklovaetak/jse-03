# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

NAME: Ekaterina Volkova

E-MAIL: avoklovaetal@yandex.ru

# SOFTWARE

* JDK 1.8

* Windows

# HARDWARE

* RAM 8Gb

* CPU i5

* HDD 128Gb

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/d/CZP_qvRs-Xm5PA?w=1